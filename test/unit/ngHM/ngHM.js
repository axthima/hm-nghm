'use strict';

// Set the jasmine fixture path
// jasmine.getFixtures().fixturesPath = 'base/';

describe('ngHM', function() {

    var module;
    var dependencies;
    dependencies = [];

    var hasModule = function(module) {
        return dependencies.indexOf(module) >= 0;
    };


    beforeEach(function() {

        // Get module
        module = angular.module('ngHM');
        dependencies = module.requires;
    });

    it('should load config module', function() {
        expect(hasModule('ngHM.config')).toBeTruthy();
    });

    

    

    
    it('should load services module', function() {
        expect(hasModule('ngHM.services')).toBeTruthy();
    });
    

});

'use strict';

// Set the jasmine fixture path
// jasmine.getFixtures().fixturesPath = 'base/';

describe.only('services.business', function() {

    var auth, business;
    var emailTest = "matthieu@tracktl.com";
    var passwordTest = "123456"

    var async = new AsyncSpec(this);

    beforeEach(module('ngHM.config'));
    beforeEach(module('ngHM.services'));

    async.beforeEach(function(done) {
        inject(function(HMAuth, HMBusiness, $q) {
            auth = HMAuth;
            business = HMBusiness;
            $q.all([auth.authReady, business.businessReady]).then(function(user) {
                auth.registerOrConnect(emailTest, passwordTest, done);
            })
        })
    });
    
    async.it('should get/create bar', function(done) {
        business.getMyBarRef(function(err, myBarRef) {
            expect(myBarRef).toBeDefined();
            var id_bar = myBarRef.name();
            auth.logout(function() {
                auth.registerOrConnect(emailTest, passwordTest, function() {
                    var myBarRef2 = business.getMyBarRef(function(err, myBarRef2) {
                        expect(myBarRef2).toBeDefined();
                        var id_bar2 = myBarRef2.name();
                        expect(id_bar).toEqual(id_bar2);
                        done();
                    });
                })
            });
        });
        
    });


    async.it('should  start HM', function(done) {
        var hmRef = business.startHM({
            duree : 10000, //ms
            nb:10,
            id_cocktail:'absolut-cosmopolitan',
        }, function(err, hmRef) {
            expect(hmRef).toBeDefined();
            done()
        });
    });

    async.it('should  protect against too much shotgun', function(done) {
        business.startHM({
            duree : 10000, //ms
            nb:3,
            id_cocktail:'absolut-cosmopolitan',
        }, function(err, newHM) {
            var nbtrys = 5;
            var success = 0;
            var tried = 0;
            function lastCheck(err) {
                tried++;
                success = err ? success : success + 1;
                if(tried == nbtrys) {
                    expect(success).toEqual(3);
                    done()
                }
            }

            for(var i=0; i<nbtrys; i=i+1) {
                business.shotGunHM(1, auth.me.id_bar, newHM.name(), lastCheck)
            }
        });
    });

     async.it('should  protect against lateness', function(done) {
        business.startHM({
            duree : 200, //ms
            nb:3,
            id_cocktail:'absolut-cosmopolitan',
        }, function(err, newHM) {

            setTimeout(function() {
                business.shotGunHM(1, auth.me.id_bar, newHM.name(), function(err) {
                    console.log(err)
                    expect(err).not.toBeNull();
                    done()
                })
            }, 1000)

        });
    });

      async.it('should  protect against toomuch', function(done) {
        business.startHM({
            duree : 20000, //ms
            nb:3,
            id_cocktail:'absolut-cosmopolitan',
        }, function(err, newHM) {

            business.shotGunHM(10, auth.me.id_bar, newHM.name(), function(err) {
                console.log(err)
                expect(err).not.toBeNull();
                done()
            })

        });
    });


      async.it('should  validate shotgun', function(done) {
        business.startHM({
            duree : 20000, //ms
            nb:3,
            id_cocktail:'absolut-cosmopolitan',
        }, function(err, newHM) {

            business.shotGunHM(1, auth.me.id_bar, newHM.name(), function(err, shotgunRef) {
                console.log(err)
                business.validateShotgun({}, auth.me.id_bar, newHM.name(), shotgunRef.name(), function() {
                    expect(err).toBeNull();
                    done()
                })
            })

        });
    });

      async.it('listenForNewHMRef fire directly', function(done) {
        business.startHM({
            duree : 3000, //ms
            nb:3,
            id_cocktail:'absolut-cosmopolitan',
        }, function(err, newHM) {

            setTimeout(function() {
                var timeSpy = jasmine.createSpy('timeSpy');
                var newRefSpy = jasmine.createSpy('timeSpy');
                business.listenForNewHMRef(auth.me.id_bar, newRefSpy, timeSpy);

                setTimeout(function() {
                    expect(timeSpy.calls.length).toEqual(2);
                    expect(newRefSpy.calls.length).toEqual(2);
                    done()
                }, 4500);
            }, 100)

        });
    });

});

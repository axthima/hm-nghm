'use strict';

// Set the jasmine fixture path
// jasmine.getFixtures().fixturesPath = 'base/';

describe.only('services.auth', function() {

    var auth;
    var emailTest = "matthieu@tracktl.com";
    var passwordTest = "123456"
    var emailTest2 = "matthieu2@tracktl.com";
    var passwordTest2 = "1234567"

    var async = new AsyncSpec(this);

    beforeEach(module('ngHM.config'));
    beforeEach(module('ngHM.services'));

    async.beforeEach(function(done) {
        inject(function(HMAuth) {
            auth = HMAuth;
            auth.authReady.then(function(user) {
                auth.removeUser(emailTest, passwordTest, done)
            })
        })
    });

    async.it('should registeruser', function(done) {
        auth.register(emailTest, passwordTest, function(err, user) {
            expect(err).toBe(null)
            expect(auth.me.is_bar).toEqual(true);
            expect(user.id).toEqual(auth.me.id);
            auth.logout(done)
        })
        
    });

    async.it('should open anonymous session', function(done) {
        auth.anonymousSession(function(err, user) {
            expect(err).toBe(null)

            expect(user.id).toEqual(auth.me.id);
            auth.logout(done)
        })
        
    });

    async.it('should connect user', function(done) {
        auth.register(emailTest, passwordTest, function(err, user) {
            expect(err).toBe(null)
            expect(user.id).toEqual(auth.me.id);
            auth.logout(function() {
                auth.connect(emailTest, passwordTest, function(err, user) {
                    expect(err).toBe(null)
                    expect(user.id).toEqual(auth.me.id);
                    auth.logout(done)
                })
            })
        })
    });

    async.it('should register or connect user', function(done) {
        auth.registerOrConnect(emailTest, passwordTest, function(err, user) {
            expect(err).toBe(null)
            expect(user.id).toEqual(auth.me.id);
            auth.logout(function() {
                auth.registerOrConnect(emailTest, passwordTest, function(err, user2) {
                    expect(err).toBe(null)
                    expect(user2.id).toBe(user.id)
                    expect(user.id).toEqual(auth.me.id);
                    auth.logout(done)
                })
            })
        })
    });

    async.it('should make change directly to me', function(done) {
        auth.registerOrConnect(emailTest, passwordTest, function(err, user) {
            auth.setMyInfos({
                test: 'pouet'
            }, function() {
                expect(auth.me.test).toEqual('pouet')
                done()
            }) 
        });
    });


});

(function(window, document) {

// Create all modules and define dependencies to make sure they exist
// and are loaded in the correct order to satisfy dependency injection
// before all nested files are concatenated by Grunt


// Config
angular.module('ngHM.config', [])
    .value('HMConfig', {
        debug: true,
        firebase : "https://happydev.firebaseio.com"
    });

// Modules
angular.module('ngHM.services', ['ngRoute']);
angular.module('ngHM.filters', []);
angular.module('ngHM',
    [
        'ngHM.config',
        'ngHM.services',
        'ngHM.filters',
        'firebase'
    ]);
angular.module('ngHM.filters', []).
    filter('duration', function () {
        return function (ms) {
            var min = Math.floor((ms/1000/60) << 0);
            var sec = Math.floor((ms/1000) % 60);
            return min + ':' + sec
        };
    });angular.module('ngHM.services')
	.service('HMAuth', ['HMConfig', '$q', '$rootScope', '$route', function(HMConf, $q, $rootScope, $route) {
		var _this = this

		_this.getUserRef = function(id_user) {
			return new Firebase(HMConf.firebase + '/users/' + id_user);
		}


	    var _authReady = $q.defer();
		var __authReady;
		_this.authReady = _authReady.promise;
		$rootScope.$on('$locationChangeStart', function(event, newUrl, oldUrl){
	        if(!__authReady) return event.preventDefault();
	    });
		var _authCallback;
	    var auth = new FirebaseSimpleLogin(new Firebase(HMConf.firebase), function(err, user) {
	        if(!__authReady) {
	        	_authCallback = function() {
	        		_authReady.resolve(user);
			        __authReady = true;
			        $route.reload();
					$rootScope.$apply();
					return
	        	}
	        }

	        _setMeUp(user, _authCallback)
		});



		_this.me = null;
		var _userRef = null;
		var _setMeUp = function(user, callback){
			if(!user) {
				_this.me = null;
				_userRef = null;
				return callback();
			};
	        
	        _userRef = _this.getUserRef(user.id);

	        _this.setMyInfos(user, callback)
		}

		_this.setMyInfos = function(infos, callback) {
			if(!_userRef)  throw new Error('you need to have a session');

			_userRef.update(infos);
			_userRef.once('value', function(snapshot) {
				_this.me = snapshot.val();
				callback(null, _this.me)
			});
		}

	    _this.logout =function(callback) {
	    	_authCallback = callback
	    	auth.logout();
	    }

	    _this.removeUser = function (email, password, callback) {
	    	auth.removeUser(email, password, _this.logout.bind(_this, callback));
	    }

		_this.register = function (email, password, callback) {
			if(_this.me!=null) {
				return callback({
					code : 'INVALID_ACTION',
					message:'you need to log out first'
				})
			}
			auth.createUser(email, password, function(err, user) {
				if(err) return callback(err);
				_this.connect(email, password, function() {
				  	_this.setMyInfos({
				  		is_bar : true
				  	}, callback)
				})
			});
		}


		_this.connect = function (email, password, callback) {
			if(_this.me!=null) {
				return callback({
					code : 'INVALID_ACTION',
					message:'you need to log out first'
				})
			}
			_authCallback = callback
			auth.login('password', {
				email : email,
				password : password,
				rememberMe : true
			});
		}

		_this.registerOrConnect = function(email, password, callback) {
			_this.register(email, password, function(err, user) {
				if(err && err.code == 'EMAIL_TAKEN') return _this.connect(email, password, callback);
				callback(err, user);
			})
		}

		_this.anonymousSession = function (callback) {
			if(_this.me!=null) {
				return callback({
					code : 'INVALID_ACTION',
					message:'you need to log out first'
				})
			}
			_authCallback = callback;
			auth.login('anonymous', {
			  rememberMe: true
			});
		}

	}]);angular.module('ngHM.services')
	.service('HMBusiness', ['HMConfig', 'HMAuth', '$q', '$rootScope', function(HMConf, HMAuth, $q, $rootScope) {
		var _this = this;

		var _businessReady = $q.defer();
		_this.businessReady = _businessReady.promise
		var _timeoffset;
		var serverTimeRef = new Firebase(HMConf.firebase + "/.info/serverTimeOffset");
		serverTimeRef.on("value", function(snap) {
		  _timeoffset = snap.val();
		  _businessReady.resolve();
		  $rootScope.$apply();
		});


		var getTempRef = function() {
			return HMAuth.getUserRef(HMAuth.me.id).child('temp');
		}

		_this.barsRef = new Firebase(HMConf.firebase + '/bars/');
		_this.getBarRef = function(id_bar) {
			return new Firebase(HMConf.firebase + '/bars/' + id_bar);
		}
		_this.getHMsRef = function(id_bar, limit) {
			var HMsRef = new Firebase(HMConf.firebase + '/bars/' + id_bar + '/hms');
			if(limit) return HMsRef.limit(limit);
			return HMsRef;
		}
		_this.getHMRef = function(id_bar, id_hm) {
			return new Firebase(HMConf.firebase + '/bars/' + id_bar + '/hms/' + id_hm);
		}

		_this.getHmShotguns = function(id_bar, id_hm) {
			return new Firebase(HMConf.firebase + '/bars/' + id_bar + '/hms/' + id_hm + '/shotguns');
		}

		_this.getHmShotgunRef = function(id_bar, id_hm, id_shotgun) {
			return new Firebase(HMConf.firebase + '/bars/' + id_bar + '/hms/' + id_hm + '/shotguns/' + id_shotgun);
		}

	    _this.getMyBarRef = function (callback) {
	    	if(!HMAuth.me || !HMAuth.me.is_bar) throw new Error('you need to be a bar')

			if(!HMAuth.me.id_bar) {
				var defaultBar = {
					name:'No Name Bar',
					id_gerant:HMAuth.me.id,
					picture: 'http://www.alcooclic.com/wp-content/uploads/2013/04/martini-bar-opera-garnier-logo1.jpg'
				};
				var barRef = _this.barsRef.push(defaultBar);
				
				HMAuth.setMyInfos({
					id_bar : barRef.name()
				}, function () {
					callback(null, barRef)
				});

				return barRef
			} else {
				callback(null, _this.getBarRef(HMAuth.me.id_bar));
			}
	    }

	    _this.startHM = function(infos, callback) {
	    	if(!HMAuth.me || !HMAuth.me.is_bar) if(!HMAuth.me || !HMAuth.me.is_bar) throw new Error('you need to be a bar')

			if(!infos.duree) throw new Error('you need to set up a duree')
			if(!infos.nb) throw new Error('you need to set up a nb')
			if(!infos.id_cocktail) throw new Error('you need to set up a id_cocktail')

			infos.start = _this.getTime();
			infos.nb_orig = infos.nb;

			var HMsRef = _this.getHMsRef(HMAuth.me.id_bar);
			var hmRef = HMsRef.push(infos);
			hmRef.once('value', function(snapshot){

				callback(null, hmRef)
			})
	    }

	    _this.getTime = function() {
	    	return new Date().getTime() + _timeoffset;
	    }

	    _this.shotGunHM = function(nb_shotgun, id_bar, id_hm, callback){

	    	var hmRef = _this.getHMRef(id_bar, id_hm);
	    	hmRef.once('value', function(snapshot){
	    		hmRef.transaction(function(currentData){
		    		if(currentData == null) return
		    		if(currentData.nb<nb_shotgun || currentData.start + currentData.duree< _this.getTime()) {
		    			return;
		    		}
		    		currentData.nb = currentData.nb - nb_shotgun;
		    		return currentData;
		    	}, function(err, commited) {
		    		if(err) return callback(err);
		    		if(!commited) return callback({code:'SHOTGUN_TOO_LATE', detail : 'You were too late!'});

		    		var HMShotgunsRef = _this.getHmShotguns(id_bar, id_hm);
		    		HMShotgunsRef.once('value', function(snapshot) {
		    			var shotgunRef = HMShotgunsRef.push({
			    			validated : false,
			    			nb_shotgun : nb_shotgun,
			    			num : snapshot.numChildren()
			    		});
			    		shotgunRef.once('value', function() {
			    			callback(null, shotgunRef)
			    		})
		    		})
		    	});
	    	})
	    }

	    _this.validateShotgun = function (infos, id_bar, id_hm, id_shotgun, callback) {
	    	var shotgunRef = _this.getHmShotgunRef(id_bar, id_hm, id_shotgun);
	    	if(infos.validated == undefined) infos.validated = true;
	    	shotgunRef.update(infos);
	    	shotgunRef.once('value', function(snapshot) {
	    		if(!snapshot.val()) return callback({code:'NON_EXISTANT'});
	    		callback();
	    	})
	    }

	    _this.listenForNewHMRef = function (id_bar, handlerref, handlertime) {
	    	var LastHMRef = _this.getHMsRef(id_bar, 1);
	    	var interval;

	    	var checkTimeLeft = function(val) {
	    		var timeleft = val.start + val.duree - _this.getTime()
	    		console.log(timeleft)
	    		if(timeleft<0 || val.nb<=0) {
	    			return false
	    		}
	    		return true
	    	}

	    	LastHMRef.on('child_added', function(snapshot) {
	    		

	    		if(interval) {
	    			clearInterval(interval);
	    			interval = null;
	    		}

	    		if(checkTimeLeft(snapshot.val())) {
	    			handlerref(snapshot.ref());
		    		interval = setInterval(function() {
		    			if(!checkTimeLeft(snapshot.val())) {
		    				clearInterval(interval);
		    				interval =null;
		    				return handlerref(null);
		    			}
		    			handlertime(snapshot.val().start + snapshot.val().duree - _this.getTime());
		    		}, 1000);
		    	} else {
		    		handlerref(null);
		    	}
	    	})

	    	return function() {
	    		LastHMRef.off('child_added')
	    	}
	    }

	}]);angular.module('ngHM.services')
	.service('HMStorage', ['$rootScope', function($rootScope) {

		var storage = {};

    	var setget = function(key, val) {
    		if(val === undefined) return storage[key]
    		storage[key] = val;
            $rootScope.$broadcast(key, val);
    	}
    	

	}]);})(window, document);
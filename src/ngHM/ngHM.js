// Create all modules and define dependencies to make sure they exist
// and are loaded in the correct order to satisfy dependency injection
// before all nested files are concatenated by Grunt


// Config
angular.module('ngHM.config', [])
    .value('HMConfig', {
        debug: true,
        firebase : "https://happydev.firebaseio.com"
    });

// Modules
angular.module('ngHM.services', ['ngRoute']);
angular.module('ngHM.filters', []);
angular.module('ngHM',
    [
        'ngHM.config',
        'ngHM.services',
        'ngHM.filters',
        'firebase'
    ]);

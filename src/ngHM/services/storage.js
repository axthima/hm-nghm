angular.module('ngHM.services')
	.service('HMStorage', ['$rootScope', function($rootScope) {

		var storage = {};

    	var setget = function(key, val) {
    		if(val === undefined) return storage[key]
    		storage[key] = val;
            $rootScope.$broadcast(key, val);
    	}
    	

	}]);
angular.module('ngHM.services')
	.service('HMAuth', ['HMConfig', '$q', '$rootScope', '$route', function(HMConf, $q, $rootScope, $route) {
		var _this = this

		_this.getUserRef = function(id_user) {
			return new Firebase(HMConf.firebase + '/users/' + id_user);
		}


	    var _authReady = $q.defer();
		var __authReady;
		_this.authReady = _authReady.promise;
		$rootScope.$on('$locationChangeStart', function(event, newUrl, oldUrl){
	        if(!__authReady) return event.preventDefault();
	    });
		var _authCallback;
	    var auth = new FirebaseSimpleLogin(new Firebase(HMConf.firebase), function(err, user) {
	        if(!__authReady) {
	        	_authCallback = function() {
	        		_authReady.resolve(user);
			        __authReady = true;
			        $route.reload();
					$rootScope.$apply();
					return
	        	}
	        }

	        _setMeUp(user, _authCallback)
		});



		_this.me = null;
		var _userRef = null;
		var _setMeUp = function(user, callback){
			if(!user) {
				_this.me = null;
				_userRef = null;
				return callback();
			};
	        
	        _userRef = _this.getUserRef(user.id);

	        _this.setMyInfos(user, callback)
		}

		_this.setMyInfos = function(infos, callback) {
			if(!_userRef)  throw new Error('you need to have a session');

			_userRef.update(infos);
			_userRef.once('value', function(snapshot) {
				_this.me = snapshot.val();
				callback(null, _this.me)
			});
		}

	    _this.logout =function(callback) {
	    	_authCallback = callback
	    	auth.logout();
	    }

	    _this.removeUser = function (email, password, callback) {
	    	auth.removeUser(email, password, _this.logout.bind(_this, callback));
	    }

		_this.register = function (email, password, callback) {
			if(_this.me!=null) {
				return callback({
					code : 'INVALID_ACTION',
					message:'you need to log out first'
				})
			}
			auth.createUser(email, password, function(err, user) {
				if(err) return callback(err);
				_this.connect(email, password, function() {
				  	_this.setMyInfos({
				  		is_bar : true
				  	}, callback)
				})
			});
		}


		_this.connect = function (email, password, callback) {
			if(_this.me!=null) {
				return callback({
					code : 'INVALID_ACTION',
					message:'you need to log out first'
				})
			}
			_authCallback = callback
			auth.login('password', {
				email : email,
				password : password,
				rememberMe : true
			});
		}

		_this.registerOrConnect = function(email, password, callback) {
			_this.register(email, password, function(err, user) {
				if(err && err.code == 'EMAIL_TAKEN') return _this.connect(email, password, callback);
				callback(err, user);
			})
		}

		_this.anonymousSession = function (callback) {
			if(_this.me!=null) {
				return callback({
					code : 'INVALID_ACTION',
					message:'you need to log out first'
				})
			}
			_authCallback = callback;
			auth.login('anonymous', {
			  rememberMe: true
			});
		}

	}]);
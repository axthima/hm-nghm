angular.module('ngHM.filters', []).
    filter('duration', function () {
        return function (ms) {
            var min = Math.floor((ms/1000/60) << 0);
            var sec = Math.floor((ms/1000) % 60);
            return min + ':' + sec
        };
    });